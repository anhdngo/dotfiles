.PHONY: all
all: pacman install-yay aur dotbot vim-plugins pyenv ohmyzsh

.PHONY: nala
nala:
	sudo nala install -y $(shell grep -vE "^\s*#" ./apt.txt | tr "\n" " ")

.PHONY: flatpak
flatpak:
	sudo flatpak remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo
	sudo flatpak install -y flathub $(shell grep -vE "^\s*#" ./flatpak.txt | tr "\n" " ")

.PHONY: oh-my-bash
oh-my-bash:
	bash -c "$(curl -fsSL https://raw.githubusercontent.com/ohmybash/oh-my-bash/master/tools/install.sh)"

.PHONY: pip
pip:
	sudo pip3 install -r pip.txt

.PHONY: dotbot
dotbot:
	git submodule update --init --recursive
	sh ./dotfiles/dotbot/bin/dotbot -c ./dotfiles/dotbot.conf.yaml

.PHONY: gitignore-global
gitignore-global:
	git config --global core.excludesfile $(shell pwd)/dotfiles/gitignore_global


.PHONY: krohnkite-settings
krohnkite-settings:
	mkdir -p ~/.local/share/kservices5/
	ln -s ~/.local/share/kwin/scripts/krohnkite/metadata.desktop ~/.local/share/kservices5/krohnkite.desktop
